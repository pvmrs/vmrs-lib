#include "vmrs/arraylist.h"

bool exportArrayList (const ArrayList & arr, string &out_file)
{
  ofstream array_file(out_file, ios::binary);

  if (array_file)
    {
      for (const auto & i : arr)
        {
          for (const auto& [bs_entry, rij] : i)
            {
              array_file.write (bs_entry.c_str (), bs_entry.size ());
              array_file.write (rij.c_str (), rij.size ());
            }
        }
      array_file.close();
      return true;
    }
  else
    {
      cout << "Error creating ArrayList file" << endl;
      return false;
    }
}


ArrayList importArrayList(const string& arr_path, const uint32_t& max, const uint32_t& bs_size, const uint32_t& rij_size, const size_t& array_size)
{
  ifstream arr_file(arr_path, ios::binary);
  ArrayList arr;
  if (arr_file.good())
    {
      uint16_t bs_byte = bit_to_byte (bs_size);
      uint16_t rij_byte = bit_to_byte (rij_size);

      for (uint32_t j = 0; j < array_size;++j)
        {
          Array cur;
          for (uint32_t i = 0; i < max; ++i)
            {
              string bs_entry, rij;
              bs_entry.resize (bs_byte);
              rij.resize (rij_byte);
              arr_file.read(bs_entry.data(), bs_byte);
              arr_file.read(rij.data(), rij_byte);
              cur.push_back ({bs_entry, rij});

            }
            arr.push_back (cur);

        }
      arr_file.close();
    }
  else
    {
      cout << "Error creating ArrayList file" << endl;
    }
  return arr;
}

void printArrayList (const ArrayList & arr, const uint32_t& h1_size, const uint32_t& param_size)
{
  cout << "ArrayList" << endl;
  for (size_t i = 0; i < arr.size();++i)
    {
      cout << "Wi: " << i << endl;
      for (const auto & [data,rij] : arr[i])
        {
          bitstring bs_data(data,h1_size);
          cout << "\tData: " << bs_data;
          bitstring bs_rij(rij,param_size);
          cout << " Rij: " << bs_rij;
          cout << endl;
        }
    }
  cout << endl;
}