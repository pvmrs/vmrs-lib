#include <iostream>
#include <fstream>
#include "vmrs/keyset.h"


using namespace std;

Keyset::Keyset(const SecByteBlock&  k1, const SecByteBlock&  k2, const SecByteBlock&  k3, const SecByteBlock&  k4, const SecByteBlock&  hash_k1, const SecByteBlock&  hash_k2, const SecByteBlock&  hash_k3, const SecByteBlock&  pk, const SecByteBlock& iv, const string& g_hash)
{
  this->k1 = k1;
  this->k2 = k2;
  this->k3 = k3;
  this->k4 = k4;
  this->pbc_k1 = hash_k1;
  this->pbc_k2 = hash_k2;
  this->pbc_k3 = hash_k3;
  this->pk = pk;
  this->iv = iv;
  this->g_hash = g_hash;
}

Keyset::Keyset()
= default;

Keyset::Keyset(const size_t &keylen, const size_t &blocksize)
{
  if (keylen%CHAR_BIT != 0 || blocksize%CHAR_BIT != 0)
    {
      cout << "Error in keylen or blocksize arguments, must be multiple of "<< CHAR_BIT << endl;
    }
  else
    {
      size_t byte_keylen = bit_to_byte (keylen);
      size_t byte_blocksize = bit_to_byte(blocksize);

      this->k1 = generate_block (byte_keylen);
      this->k2 = generate_block (byte_keylen);
      this->k3 = generate_block (byte_keylen);
      this->k4 = generate_block (byte_keylen);
      this->pbc_k1 = generate_block (byte_keylen);
      this->pbc_k2 = generate_block (byte_keylen);
      this->pbc_k3 = generate_block (byte_keylen);
      this->pk = generate_block (byte_keylen);
      this->iv = generate_block (byte_blocksize);
      this->g_hash = get_random(keylen).to_string ();
      //this->g = G1(*p, false);
    }

}

void Keyset::exportKeys(const string &file, const string &ghash_file)
{
  ofstream key_file(file, ios::binary);
  ofstream hash_file(ghash_file, ios::binary);
  if (key_file.good() && hash_file.good())
    {
      key_file.write (reinterpret_cast<char*>(this->k1.data ()), k1.size());
      key_file.write (reinterpret_cast<char*>(this->k2.data ()), k2.size());
      key_file.write (reinterpret_cast<char*>(this->k3.data ()), k3.size());
      key_file.write (reinterpret_cast<char*>(this->k4.data ()), k4.size());
      key_file.write (reinterpret_cast<char*>(this->pbc_k1.data ()), pbc_k1.size());
      key_file.write (reinterpret_cast<char*>(this->pbc_k2.data ()), pbc_k2.size());
      key_file.write (reinterpret_cast<char*>(this->pbc_k3.data ()), pbc_k3.size());
      key_file.write (reinterpret_cast<char*>(this->pk.data ()), pk.size());
      key_file.write (reinterpret_cast<char*>(this->iv.data ()), iv.size());
      key_file.write (reinterpret_cast<char*>(this->g_hash.data ()), g_hash.size());

      key_file.close();
      hash_file.write (reinterpret_cast<char*>(this->g_hash.data ()), g_hash.size());
      hash_file.close();

    }
  else
    {
      cerr << "Error creating key file." << endl;
      exit(1);
    }
}

Keyset Keyset::importKeys (const string &file, const size_t &keylen, const size_t &blocksize)
{

  if (keylen%CHAR_BIT != 0 || blocksize%CHAR_BIT != 0)
    {
      cout << "Error in keylen or blocksize arguments, must be multiple of "<< CHAR_BIT << endl;
      return Keyset();
    }

  size_t byte_keylen = bit_to_byte(keylen);
  size_t byte_blocksize = bit_to_byte(blocksize);


  ifstream key_file(file, ios::binary);
  if (key_file.good ())
    {

      char * key_buffer = new char [keylen];
      char * iv_buffer = new char [blocksize];

      key_file.read(key_buffer, byte_keylen);
      SecByteBlock new_k1((const unsigned char*)key_buffer,byte_keylen);
      key_file.read(key_buffer, byte_keylen);
      SecByteBlock new_k2((const unsigned char*)key_buffer,byte_keylen);
      key_file.read(key_buffer, byte_keylen);
      SecByteBlock new_k3((const unsigned char*)key_buffer,byte_keylen);
      key_file.read(key_buffer, byte_keylen);
      SecByteBlock new_k4((const unsigned char*)key_buffer,byte_keylen);
      key_file.read(key_buffer, byte_keylen);
      SecByteBlock new_hash_k1((const unsigned char*)key_buffer,byte_keylen);
      key_file.read(key_buffer, byte_keylen);
      SecByteBlock new_hash_k2((const unsigned char*)key_buffer,byte_keylen);
      key_file.read(key_buffer, byte_keylen);
      SecByteBlock new_hash_k3((const unsigned char*)key_buffer,byte_keylen);
      key_file.read(key_buffer, byte_keylen);
      SecByteBlock new_pk((const unsigned char*)key_buffer,byte_keylen);

      key_file.read(iv_buffer, byte_blocksize);
      SecByteBlock new_iv((const unsigned char*)iv_buffer,byte_blocksize);


      key_file.read(iv_buffer, byte_blocksize);
      string g_hash(iv_buffer,byte_blocksize);

      key_file.close();

      return Keyset(new_k1, new_k2, new_k3, new_k4, new_hash_k1, new_hash_k2, new_hash_k3, new_pk, new_iv, g_hash);
    }
  else
    {
        cerr << "Error opening key file." << endl;
        exit(1);
    }
}




SecByteBlock Keyset::getPK() const
{
  return pk;
}

SecByteBlock Keyset::getIV() const
{
  return iv;
}

SecByteBlock Keyset::getK1() const
{
  return k1;
}

SecByteBlock Keyset::getK2() const
{
  return k2;
}

SecByteBlock Keyset::getK3() const
{
  return k3;
}

SecByteBlock Keyset::getK4() const
{
  return k4;
}

SecByteBlock Keyset::getPbcK1 () const
{
  return pbc_k1;
}

SecByteBlock Keyset::getPbcK2 () const
{
  return pbc_k2;
}

SecByteBlock Keyset::getPbcK3 () const
{
  return pbc_k3;
}

void Keyset::printKeys()
{
  cout << "K1: " << sec_byte_block_to_hex (this->k1) << endl;
  cout << "K2: " << sec_byte_block_to_hex (this->k2) << endl;
  cout << "K3: " << sec_byte_block_to_hex (this->k3) << endl;
  cout << "K4: " << sec_byte_block_to_hex (this->k4) << endl;
  cout << "PBC K1: " << sec_byte_block_to_hex (this->pbc_k1) << endl;
  cout << "PBC k2: " << sec_byte_block_to_hex (this->pbc_k2) << endl;
  cout << "PBC k3: " << sec_byte_block_to_hex (this->pbc_k3) << endl;
  cout << "PK: " << sec_byte_block_to_hex (this->pk) << endl;
  cout << "IV: " << sec_byte_block_to_hex (this->iv) << endl;
  cout << "G HASH: " << to_hex(this->g_hash) << endl;
}

string Keyset::getGHash () const
{
  return g_hash;
}
