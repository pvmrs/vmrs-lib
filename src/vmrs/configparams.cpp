#include "vmrs/configparams.h"
#include "libconfig.h++"

using namespace std;
using namespace libconfig;


ConfigParams* ConfigParams::instance = nullptr;

ConfigParams* ConfigParams::getInstance()
{
    if (instance == nullptr)
    {
        instance = new ConfigParams();
    }

    return instance;
}

ConfigParams* ConfigParams::getInstance(const std::string& cfgpath)
{
    if (instance == nullptr)
    {
        instance = new ConfigParams(cfgpath);
    }

    return instance;
}


ConfigParams::ConfigParams(const std::string& cfgpath)
{
    // Read the file. If there is an error, report it and exit.
    try
    {
        lcfg.readFile(cfgpath.data());
    }
    catch(const FileIOException &fioex)
    {
        std::cerr << "I/O error while reading config file." << std::endl;
        exit(1);
    }
    catch(const ParseException &pex)
    {
        std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
                  << " - " << pex.getError() << std::endl;
        exit(1);
    }
}

ConfigParams::ConfigParams()
{
  // Read the file. If there is an error, report it and exit.
  try
  {
    lcfg.readFile("config.txt");
  }
  catch(const FileIOException &fioex)
  {
    std::cerr << "I/O error while reading config file." << std::endl;
  }
  catch(const ParseException &pex)
  {
    std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError() << std::endl;
  }
}

bool ConfigParams::getParam(const std::string &path, std::string &value) const
{
  if(!lcfg.lookupValue(path, value))
  {
    cerr << "No '" << path << "' setting in configuration file." << endl;
    return false;
  }
  else
  {
    return true;
  }
}

bool ConfigParams::getParam(const std::string &path, int &value) const
{
  if(!lcfg.lookupValue(path, value))
  {
    cerr << "No '" << path << "' setting in configuration file." << endl;
    return false;
  }
  else
  {
    return true;
  }
}
