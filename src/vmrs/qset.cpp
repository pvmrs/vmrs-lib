#include "vmrs/qset.h"

void printQSet (const QSet& qs, const uint32_t& key_size, const uint32_t& value_size)
{
  cout << "QSet" << endl;
  for (const auto& [key, v] : qs)
    {
      bitstring bs_key(key, key_size);
      bitstring bs_value(v, value_size);
      cout << "Key: " << bs_key << " Value: " << bs_value << endl;
    }
  cout << endl;
}

bool exportQSet(const QSet& qs, const string& qs_path)
{
  ofstream qs_file(qs_path, ios::binary);
  if (qs_file)
    {
      for (const auto& [key, v] : qs)
        {
          string qs_entry = key + v;
          qs_file.write (qs_entry.c_str (), qs_entry.size ());
        }

      qs_file.close();
      return true;
    }
  else
    {
      cout << "Error creating QSet file" << endl;
      return false;
    }
}

QSet importQSet(const string& qs_path, const uint32_t& key_size, const uint32_t& value_size)
{
  ifstream qs_file(qs_path, ios::binary);
  QSet qs;
  if (qs_file)
    {
      uint16_t key_byte = bit_to_byte (key_size);
      uint16_t value_byte = bit_to_byte (value_size);

      qs_file.seekg (0, qs_file.end);
      int length = qs_file.tellg();
      qs_file.seekg (0, qs_file.beg);

      uint32_t qs_size = length/(key_byte + value_byte);

      for (uint32_t j = 0;j<qs_size;++j)
        {
          string key, value;
          key.resize(key_byte);
          value.resize(value_byte);
          qs_file.read(key.data(), key_byte);
          qs_file.read(value.data(), value_byte);
          qs.insert ({key, value});
        }
      qs_file.close();
    }
  else
    {
      cout << "Error creating QSet file" << endl;
    }
  return qs;
}