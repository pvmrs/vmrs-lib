#include "vmrs/vmrssizes.h"

VMRSSizes::VMRSSizes()
{
}

void VMRSSizes::exportSizes (const string &file_path)
{
  ofstream out_file(file_path);
  if (out_file.is_open())
    {
      out_file << dic_size << endl;
      out_file << dataset_size << endl;
      out_file << score_size << endl;
      out_file << array_size << endl;
      out_file << h1_size << endl;
      out_file << f_size << endl;
      out_file << param_size << endl;
      out_file << l_size << endl;
      out_file << pi_size << endl;
      out_file << zp_size << endl;
      out_file << fid_size << endl;
      out_file << zr_size << endl;
      out_file << max << endl;

      out_file.close();
    }
  else
    {
      cout << "Error creating VMRS sizes file." << endl;
    }
}
void VMRSSizes::importSizes (const string &file_path)
{
  ifstream in_file(file_path);
  if (in_file.is_open())
    {
      in_file >> dic_size;
      in_file >> dataset_size;
      in_file >> score_size;
      in_file >> array_size;
      in_file >> h1_size;
      in_file >> f_size;
      in_file >> param_size;
      in_file >> l_size;
      in_file >> pi_size;
      in_file >> zp_size;
      in_file >> fid_size;
      in_file >> zr_size;
      in_file >> max;

      in_file.close();
    }
  else
    {
      cout << "Error importing VMRS sizes file." << endl;
    }
}

void VMRSSizes::printSizes() const
{
  cout << "Sizes (bits)" << endl;
  cout << "Dictionary size (m): " << dic_size << endl;
  cout << "Dataset size (n): " << dataset_size << endl; //n
  cout << "Score size (b): " << score_size << endl; //TFxIDF size (b)
  cout << "Array size (2^d, d > log m): " << array_size << endl; // 2^d, d > log m
  cout << "h1 size (log2(n) + b + log2(pairing-1) + 1): " << h1_size << endl; //log2(n) + b + log2(pairing-1) + 1
  cout << "f size (log2(u)): " << f_size << endl; //log2(u)
  cout << "lambda size: " << param_size << endl; //lambda
  cout << "l size (using lambda): " << l_size << endl; //lambda
  cout << "pi size (d): " << pi_size << endl; //d
  cout << "Zp size (G1): " << zp_size << endl << endl;
}
