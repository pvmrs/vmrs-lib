#include "bitstring.h"

vmrs::bitstring::bitstring() = default;

vmrs::bitstring::bitstring(const string& data, size_t size) {
    this->data = bs(data.begin (), data.end());
    this->data.resize (size,false);
}

vmrs::bitstring::bitstring(const string& data) {
    this->data = bs(data.begin (), data.end());
}

vmrs::bitstring::bitstring(ulong data, size_t size) {
    this->data = bs(size, data);
}

vmrs::bitstring::bitstring(ulong data) {
  this->data = bs(sizeof(data)*CHAR_BIT, data);
}

vmrs::bitstring::bitstring(int data, size_t size) {
    this->data = bs(size, data);
}

vmrs::bitstring::bitstring(int data) {
  this->data = bs(sizeof(data)*CHAR_BIT, data);
}

vmrs::bitstring::bitstring(float data, size_t size) {
  this->data = bs(size,*reinterpret_cast<unsigned long *>(&data));
}

vmrs::bitstring::bitstring(float data) {
    this->data = bs(sizeof(data)*CHAR_BIT,*reinterpret_cast<unsigned long *>(&data));
}

string vmrs::bitstring::to_string() const {
  string out_str;
  out_str.reserve(this->data.num_blocks());

  boost::to_block_range(this->data, std::back_inserter(out_str));
  return out_str;
}

size_t vmrs::bitstring::size() const { return this->data.size(); }

size_t vmrs::bitstring::blocks() const { return this->data.num_blocks (); }

string vmrs::bitstring::get_bits() const {
  string out;
  boost::to_string (this->data, out);
  return out;
}

ulong vmrs::bitstring::to_ulong() const {
  bool fits_in_ulong = true;
  unsigned long ul = 0;
  try {
      ul = this->data.to_ulong();
    } catch(std::overflow_error &) {
      std::cerr << this->data.size () << " does not fit in ulong" << std::endl;
      exit(1);
    }

    return ul;
}

int vmrs::bitstring::to_int() const {
  if (this->data.size() > sizeof(int)*CHAR_BIT)
    {
      std::cerr << this->data.size () << " does not fit in ulong" << std::endl;
      exit(1);
    }
  return (int)this->data.to_ulong();
}

float vmrs::bitstring::to_float() const {
  if (this->data.size() > sizeof(float)*CHAR_BIT)
    {
      std::cerr << this->data.size () << " does not fit in float" << std::endl;
      exit(1);
    }
    ulong a = this->data.to_ulong();
  float out =  *reinterpret_cast<float *>(&a);
  return out;
}

vmrs::bitstring::bitstring(const char *data, size_t size) : vmrs::bitstring::bitstring (string(data), size) {}

vmrs::bitstring vmrs::bitstring::substring(size_t len) {
    bitstring nbs = *this;
    nbs.resize (len);
    return nbs;
}

vmrs::bitstring vmrs::bitstring::substring(size_t start, size_t len) {
    assert(start < this->data.size());

    if (len > this->data.size()) {
        len = this->data.size();
    }

    bs out(len);

    for (size_t i = start; i < (start+len); ++i)
      {
        out[i - start] = this->data[i];
      }

    return out;
}
vmrs::bitstring::bitstring (bool data, size_t size)
{
  bs aux;
  this->data.resize(size, data);
}

vmrs::bitstring::bitstring (bool data)
{
  this->data.resize (1, data);
}
vmrs::bitstring::bitstring (const char *data) : bitstring(string(data)){}

void vmrs::bitstring::resize (const size_t& n)
{
  this->data.resize (n);
}

vmrs::bitstring::bitstring (bs a)
{
  this->data = a;
}

void vmrs::bitstring::set (const size_t &i, bool v)
{
  this->data[i] = v;
}
