#include "vmrs/postinglist.h"
#include <cmath>

DocumentItem::DocumentItem()
{
}

unsigned long DocumentItem::getFid() const
{
  return fid;
}

void DocumentItem::setFid(const unsigned long &value)
{
  fid = value;
}

float DocumentItem::getScore() const
{
  return score;
}

void DocumentItem::setScore (float value)
{
  score = value;
}

int DocumentItem::getOccurrences() const
{
  return occurrences;
}

void DocumentItem::setOccurrences(int value)
{
  occurrences = value;
}

string DocumentItem::getW() const
{
  return w;
}

void DocumentItem::setW(const string &value)
{
  w = value;
}

float DocumentItem::calculateScore(float norm_factor, int dataset_size, int docs_w)
{
  //TF: number oftimes that a given keyword appears in a document
  float tf = (1 + log(this->occurrences))/norm_factor;

  //IDF: division of the cardinality of the document collection by the number of documents matching the keyword
  float idf = log(1 + (dataset_size/docs_w));

  this->score = tf*idf;
  return this->score;
}

string DocumentItem::getY() const
{
  return y;
}

void DocumentItem::setY(const string &value)
{
  y = value;
}

void printPostingList (const PostingList & pl)
{
  cout << "Posting List" << endl;
  cout << "Keyword\t File\t Occur\t Score " << endl;
  for (const auto& cur_pl : pl)
    {
      cout << cur_pl.first << endl;
      for (const auto& cur_ds : cur_pl.second)
        {
          cout << "\t\t" << cur_ds.getFid() << "\t" << cur_ds.getOccurrences() << "\t" << cur_ds.getScore() << endl;
        }
    }
  cout << endl;
}