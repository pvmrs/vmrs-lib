#include "vmrs/vmrs.h"

size_t vmrs::bit_to_byte(const size_t& bit)
{
    return bit/CHAR_BIT + ((bit%CHAR_BIT == 0) ? 0 : 1);
    return ceil((double)bit/CHAR_BIT);
}

vmrs::bitstring vmrs::hmac(const SecByteBlock& key, const std::string& text, size_t size)
{
    string mac;
    HMAC< SHA256 > hmac(key, key.size());

    StringSource ss(text, true,
                    new CryptoPP::HashFilter(hmac,
                                             new StringSink(mac)
                    ) // HashFilter
    ); // StringSource

    assert(mac.size ()*CHAR_BIT >= size);

    return vmrs::bitstring(mac,size);
}

vmrs::bitstring vmrs::hmac(const SecByteBlock& key, const std::string& text)
{
    string mac;
    HMAC< SHA256 > hmac(key, key.size());

    StringSource ss(text, true,
                    new CryptoPP::HashFilter(hmac,
                                             new StringSink(mac)
                    ) // HashFilter
    ); // StringSource

    return vmrs::bitstring(mac);
}

string vmrs::hmac_str(const SecByteBlock& key, const std::string& text, size_t size)
{
    string mac;
    HMAC< SHA256 > hmac(key, key.size());

    StringSource ss(text, true,
                    new CryptoPP::HashFilter(hmac,
                                             new StringSink(mac)
                    ) // HashFilter
    ); // StringSource

    size_t byte_size = bit_to_byte(size);
    assert(mac.size () >= byte_size);
    mac.resize (byte_size);

    return mac;
}

string vmrs::hmac_str(const SecByteBlock& key, const std::string& text)
{
    string mac;
    HMAC< SHA256 > hmac(key, key.size());

    StringSource ss(text, true,
                    new CryptoPP::HashFilter(hmac,
                                             new StringSink(mac)
                    ) // HashFilter
    ); // StringSource
    return mac;
}

string vmrs::to_hex(const string& in)
{
    string encoded;
    encoded.clear();

    StringSource (in, true,
                  new HexEncoder(
                          new StringSink(encoded)
                  ) // HexEncoder
    ); // StringSource

    return encoded;
}

string vmrs::exp_pbc(Pairing *p, const G1& g, const string& in)
{
    Zr el_exp(*p, in.c_str(), in.length());
    G1 result(g^el_exp);

    return result.toString(false);
}

string vmrs::divide_zr(const string& lhs, const string& rhs)
{
    Zr z_lhs(lhs);
    Zr z_rhs(rhs);
    Zr result(z_lhs/z_rhs);
    return result.toString();
}

string vmrs::prf_group(Pairing *p, const SecByteBlock& key, const string& in)
{
    std::string hash = vmrs::hmac_str(key, in);
    Zr result(*p, hash.data(), hash.length());
    return result.toString();
}

vmrs::bitstring vmrs::hash(const string& text, const size_t& size)
{
    SHA256 hash;
    string digest;

    StringSource ss(text, true, new CryptoPP::HashFilter(hash, new StringSink(digest)));

    return vmrs::bitstring(digest,size);
}

Zr vmrs::from_bs (Pairing *p, const vmrs::bitstring& in)
{
    string str_in = in.to_string();
    return Zr(*p, str_in.c_str(), str_in.length());
}

vmrs::bitstring vmrs::get_random (const size_t& size)
{
    AutoSeededRandomPool prng;
    SecByteBlock out(bit_to_byte (size));
    prng.GenerateBlock(out, out.size());
    std::string random_str(reinterpret_cast<const char*>(out.data()), out.size());
    return vmrs::bitstring (random_str, size);
}

string vmrs::enc_aes_ctr (const SecByteBlock& key, const SecByteBlock& iv, const string& plain)
{
    try
    {
        CTR_Mode< AES >::Encryption e;
        e.SetKeyWithIV(key, key.size(), iv);

        string cipher;
        StringSource (plain, true,
                      new StreamTransformationFilter(e,
                                                     new StringSink(cipher)
                      ) //StreamTransformationFilter
        ); //StringSource

        return cipher;
    }
    catch(const CryptoPP::Exception& e)
    {
        cerr << e.what () << endl;
        exit (1);
    }
}

vmrs::bitstring vmrs::enc_aes_ctr (const SecByteBlock& key, const SecByteBlock& iv, const vmrs::bitstring &plain)
{
    string plain_str = plain.to_string();
    string out_str = vmrs::enc_aes_ctr (key, iv, plain_str);
    return vmrs::bitstring (out_str, plain.size());
}

string vmrs::dec_aes_ctr (const SecByteBlock& key, const SecByteBlock& iv, const string& cipher)
{
    try
    {
        CTR_Mode< AES >::Decryption e;
        e.SetKeyWithIV(key, key.size(), iv);

        string nplain;
        StringSource (cipher, true,
                      new StreamTransformationFilter(e,
                                                     new StringSink(nplain)
                      ) // StreamTransformationFilter
        ); // StringSource

        return nplain;
    }
    catch(const CryptoPP::Exception& e)
    {
        cerr << e.what() << endl;
        exit(1);
    }
}

vmrs::bitstring vmrs::dec_aes_ctr (const SecByteBlock& key, const SecByteBlock& iv, const vmrs::bitstring &cipher)
{
    string cipher_str = cipher.to_string();
    string out_str = vmrs::dec_aes_ctr (key, iv, cipher_str);
    return vmrs::bitstring (out_str, cipher.size());
}

SecByteBlock vmrs::generate_block(const size_t& size)
{
    SecByteBlock block(size);
    AutoSeededRandomPool prng;
    prng.GenerateBlock(block, block.size());
    return block;
}

string vmrs::sec_byte_block_to_hex(const SecByteBlock &in)
{
    string encoded;

    encoded.clear();
    StringSource ss(in, in.size(), true,
                    new HexEncoder(
                            new StringSink(encoded)
                    ) // HexEncoder
    ); // StringSource

    return encoded;
}

void vmrs::ltrim(std::string &s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
      return !std::isspace(ch);
  }));
}

// trim from end (in place)
void vmrs::rtrim(std::string &s) {
  s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
      return !std::isspace(ch);
  }).base(), s.end());
}

// trim from both ends (in place)
void vmrs::trim(std::string &s) {
  ltrim(s);
  rtrim(s);
}

void vmrs::to_lower(std::string &s) {
  transform(s.begin(), s.end(), s.begin(), ::tolower);
}