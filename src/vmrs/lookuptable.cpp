#include "vmrs/lookuptable.h"

bool exportLookupTable(const LookupTable& lt, const string& lt_path)
{
  ofstream lt_file(lt_path, ios::binary);
  if (lt_file)
    {
      for (const auto& [key, v] : lt)
        {
          string lt_entry = key + v.first + v.second;
          lt_file.write (lt_entry.c_str (), lt_entry.size ());
        }

      lt_file.close();
      return true;
    }
  else
    {
      cout << "Error creating LookupTable file" << endl;
      return false;
    }
}


LookupTable importLookupTable(const string& lt_path, const uint32_t& key_size, const uint32_t& addr_size, const uint32_t& dataset_size, const uint32_t& l_size)
{
  ifstream lt_file(lt_path, ios::binary);
  LookupTable lt;
  if (lt_file)
    {
      uint16_t key_byte = bit_to_byte (key_size);
      uint16_t addr_byte = bit_to_byte (addr_size);
      uint16_t tag_byte = bit_to_byte (dataset_size) + bit_to_byte (l_size);

      lt_file.seekg (0, lt_file.end);
      int length = lt_file.tellg();
      lt_file.seekg (0, lt_file.beg);

      uint32_t lt_size = length/(key_byte + addr_byte + tag_byte);
      for (uint32_t j = 0;j<lt_size;++j)
        {
          string key, addr, tag;
          key.resize(key_byte);
          addr.resize (addr_byte);
          tag.resize (tag_byte);
          lt_file.read(key.data(), key_byte);
          lt_file.read(addr.data(), addr_byte);
          lt_file.read(tag.data(), tag_byte);

          lt.insert ({key, {addr, tag}});
        }
      lt_file.close();
    }
  else
    {
      cout << "Error creating LookupTable file" << endl;
    }
  return lt;
}

void printLookupTable (const LookupTable& lt, const uint32_t& key_size, const uint32_t& addr_size, const uint32_t& dataset_size, const uint32_t& l_size)
{
  cout << "LookupTable" << endl;

  uint32_t tag_size = bit_to_byte (dataset_size)*8 + l_size;
  for (const auto& [key, v] : lt)
    {
      bitstring bs_key(key, key_size);
      bitstring bs_f(v.first, addr_size);
      bitstring bs_s(v.second, tag_size);

      cout << "Key: " << bs_key;
      cout << " Addr: " << bs_f;
      cout << " Tag: " << bs_s << endl;
    }
  cout << endl;
}
pLookupTable::pLookupTable (const VMRSSizes &vs, const string &lt_path)
{
  ifstream lt_file(lt_path, ios::binary);
  if (lt_file)
    {
      uint16_t key_byte = bit_to_byte (vs.pi_size);
      uint16_t addr_byte = bit_to_byte (vs.f_size);
      uint16_t tag_byte = bit_to_byte (vs.dataset_size) + bit_to_byte (vs.l_size);

      lt_file.seekg (0, lt_file.end);
      int length = lt_file.tellg();
      lt_file.seekg (0, lt_file.beg);

      uint32_t lt_size = length/(key_byte + addr_byte + tag_byte);

      for (uint32_t j = 0;j<lt_size;++j)
        {
          string key, addr, tag;
          ulong size;

          key.resize(key_byte);
          addr.resize (addr_byte);
          tag.resize (tag_byte);

          lt_file.read(key.data(), key_byte);
          lt_file.read(addr.data(), addr_byte);
          lt_file.read((char*)&size, sizeof(size));
          lt_file.read(tag.data(), tag_byte);

          lt.insert ({key, {addr, size, tag}});
        }
      lt_file.close();

      key_size = vs.pi_size;
      addr_size = vs.f_size;
      tag_vec_size = vs.dataset_size;
      tag_mac_size = vs.l_size;
    }
  else
    {
      cout << "Error importing pLookupTable" << endl;
    }
}

bool pLookupTable::save (const string &lt_path)
{
  ofstream lt_file(lt_path, ios::binary);
  if (lt_file)
    {
      for (const auto& [key, v] : lt)
        {
          lt_file.write (key.c_str (), key.size ());
          lt_file.write (v.addr.c_str (), v.addr.size ());
          lt_file.write (reinterpret_cast<const char *>(&v.size), sizeof(v.size));
          lt_file.write (v.tag.c_str (), v.tag.size ());
        }

      lt_file.close();
      return true;
    }
  else
    {
      cout << "Error creating LookupTable file" << endl;
      return false;
    }
}

void pLookupTable::print ()
{
  cout << "LookupTable" << endl;

  uint32_t tag_size = bit_to_byte (tag_vec_size)*8 + tag_mac_size;
  for (const auto& [key, v] : lt)
    {
      bitstring bs_key(key, key_size);
      bitstring bs_f(v.addr, addr_size);
      bitstring bs_s(v.tag, tag_size);

      cout << "Key: " << bs_key;
      cout << " Addr: " << bs_f;
      cout << " Size: " << v.size;
      cout << " Tag: " << bs_s << endl;
    }
  cout << endl;
}