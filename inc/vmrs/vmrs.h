#ifndef _VMRS_H_
#define _VMRS_H_

#include "bitstring.h"

#include <bitset>
#include <climits>
#include <cmath>
#include <algorithm>
#include <cctype>

#include <crypto++/hex.h>
using CryptoPP::HexEncoder;

#include <crypto++/filters.h>
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include <crypto++/sha.h>
using CryptoPP::SHA256;

#include <crypto++/hmac.h>
using CryptoPP::HMAC;

#include <crypto++/osrng.h>
using CryptoPP::AutoSeededRandomPool;

#include <crypto++/aes.h>
using CryptoPP::AES;

#include <crypto++/secblock.h>
using CryptoPP::SecByteBlock;

#include <crypto++/ccm.h>
using CryptoPP::CTR_Mode;

#include <pbcpp/PBC.h>

namespace vmrs
{
    bitstring hmac(const SecByteBlock& key, const std::string& text, size_t size);
    bitstring hmac(const SecByteBlock& key, const std::string& text);
    std::string hmac_str(const SecByteBlock& key, const std::string& text, size_t size);
    std::string hmac_str(const SecByteBlock& key, const std::string& text);
    std::string to_hex(const std::string& in);
    std::string exp_pbc(Pairing *p, const G1& g, const std::string& in);
    std::string divide_zr(const std::string& lhs, const std::string& rhs);
    std::string prf_group(Pairing *p, const SecByteBlock& key, const std::string& in);
    bitstring hash(const std::string& text, const size_t& size);

    Zr from_bs(Pairing *p, const bitstring& in);
    bitstring get_random(const size_t& size);
    std::string enc_aes_ctr(const SecByteBlock& key, const SecByteBlock& iv, const std::string &plain);
    bitstring enc_aes_ctr(const SecByteBlock& key, const SecByteBlock& iv, const bitstring &plain);
    std::string dec_aes_ctr (const SecByteBlock& key, const SecByteBlock& iv, const std::string &cipher);
    bitstring dec_aes_ctr (const SecByteBlock& key, const SecByteBlock& iv, const bitstring &cipher);
    SecByteBlock generate_block(const size_t& size);
    std::string sec_byte_block_to_hex(const SecByteBlock &in);
    size_t bit_to_byte (const size_t& bits);

    void ltrim (std::string &s);
    void rtrim (std::string &s);
    void trim (std::string &s);
    void to_lower(std::string &s);
}

#endif //_VMRS_H_
