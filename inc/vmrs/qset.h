#ifndef _QSET_H_
#define _QSET_H_

#include <iostream>
#include <unordered_map>
#include <fstream>
#include <vmrs/vmrs.h>

using namespace std;
using namespace vmrs;

using QSet = unordered_map<string,string>;

void printQSet (const QSet& lt, const uint32_t& key_size, const uint32_t& value_size);
QSet importQSet(const string& qs_path, const uint32_t& key_size, const uint32_t& value_size);
bool exportQSet(const QSet& qs, const string& qs_path);

#endif //_QSET_H_
