#ifndef KEY_H
#define KEY_H

#include <iostream>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include "vmrs/vmrs.h"

using namespace std;
using namespace vmrs;

class Keyset
{
public:
  Keyset(const SecByteBlock& k1, const SecByteBlock& k2, const SecByteBlock& k3,
      const SecByteBlock& k4, const SecByteBlock& hash_k1, const SecByteBlock& hash_k2, const SecByteBlock& hash_k3, const SecByteBlock& pk, const SecByteBlock &iv, const string &g_hash);
  //IN BITS
  Keyset(const size_t &keylen, const size_t &blocksize);
  Keyset();
  SecByteBlock getPK() const;
  SecByteBlock getIV() const;
  SecByteBlock getK1() const;
  SecByteBlock getK2() const;
  SecByteBlock getK3() const;
  SecByteBlock getK4() const;
  SecByteBlock getPbcK1 () const;
  SecByteBlock getPbcK2 () const;
  SecByteBlock getPbcK3 () const;
  string getGHash () const;

  void printKeys();
  void exportKeys (const string& file, const string &ghash_file);
  //in bits
  static Keyset importKeys(const string& file, const size_t &keylen, const size_t &blocksize);
private:
  SecByteBlock k1,k2,k3,k4,pbc_k1,pbc_k2,pbc_k3;
  SecByteBlock pk,iv;
  string g_hash;

};

#endif // KEY_H
