#ifndef _LOOKUPTABLE_H_
#define _LOOKUPTABLE_H_

#include <iostream>
#include <unordered_map>
#include <fstream>
#include <utility>
#include <vmrs/vmrs.h>
#include <vmrs/vmrssizes.h>

using namespace std;
using namespace vmrs;

using LookupTable = unordered_map<string,pair<string,string>>;

bool exportLookupTable(const LookupTable& lt, const string& lt_path);
LookupTable importLookupTable(const string& lt_path, const uint32_t& key_size, const uint32_t& addr_size, const uint32_t& dataset_size, const uint32_t& l_size);
void printLookupTable (const LookupTable& lt, const uint32_t& key_size, const uint32_t& addr_size, const uint32_t& dataset_size, const uint32_t& l_size);

class pLookupTable {
 public:

  class pLookupTableItem {
   public:
    pLookupTableItem(string naddr, const ulong& nsize, string  ntag)
        : addr(std::move(naddr)), size(nsize), tag(std::move(ntag)) {}
    string addr;
    string tag;
    unsigned long size;
  };

  pLookupTable(const VMRSSizes& vs, const string& lt_path);
  pLookupTable(size_t key_size, size_t addr_size, size_t tag_vec_size, size_t tag_mac_size)
    : key_size(key_size), addr_size(addr_size), tag_vec_size(tag_vec_size), tag_mac_size(tag_mac_size) {}


  bool save(const string& lt_path);
  void print();
  size_t size(){return lt.size();};
  void insert(const string& key, const pLookupTableItem& item){lt.insert({key, item});}
  pLookupTableItem get(const string& k) const { return lt.at(k);}
  unordered_map<string,pLookupTableItem>::const_iterator find (const string& k) const {return lt.find(k);}
  unordered_map<string,pLookupTableItem>::const_iterator end() {return lt.end();}


 private:
  unordered_map<string,pLookupTableItem> lt;
  size_t key_size, addr_size, tag_vec_size, tag_mac_size;
};




#endif //_LOOKUPTABLE_H_
