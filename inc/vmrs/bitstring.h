#ifndef _BITSTRING_H_
#define _BITSTRING_H_

#include <iostream>
#include <climits>
#include <cassert>
#include <boost/dynamic_bitset.hpp>

using namespace std;
using bs = boost::dynamic_bitset<unsigned char>;

namespace vmrs
{
 class bitstring {
    public:
      bitstring();
      bitstring (const string& data, size_t size);
      bitstring(bs data);
      explicit bitstring (const string& data);
      bitstring (bool data, size_t size);
      explicit bitstring (bool data);
      bitstring (ulong data, size_t size);
      explicit bitstring (ulong data);
      bitstring (float data, size_t size);
      explicit bitstring (float data);
      bitstring (int data, size_t size);
      explicit bitstring (int data);
      bitstring (const char * data, size_t size);
      explicit bitstring (const char * data);

      bool operator== (const bitstring &rhs) const
      {
        return (data == rhs.data);
      }

      bool operator!= (const bitstring &rhs) const
      {
        return (data != rhs.data);
      }

      bitstring &operator+= (const bitstring &rhs)
      {
        size_t lsize = data.size();
        size_t rsize = rhs.data.size ();
        data.resize (lsize + rsize);

        for (size_t i = 0; i < rsize; i++)
          data[i + lsize] = rhs.data[i];

        return *this;
      }

      friend bitstring operator+ (bitstring lhs, const bitstring &rhs)
      {
        lhs += rhs;
        return lhs;
      }

      //bool operator [](int i) const;
      bool operator[](size_t i) const{return this->data[i];};
      //const bool & operator[](size_t i) {return data[i];};

      bitstring &operator^= (const bitstring &rhs)
      {
        data^=rhs.data;
        return *this;
      }

      friend bitstring operator^ (bitstring lhs, const bitstring &rhs)
      {
        lhs ^= rhs;
        return lhs;
      }

      friend ostream& operator<<(ostream& os, const bitstring& bs)
      {
          return os << bs.data;
      }


      string get_bits() const;
      size_t size () const;
      size_t blocks () const;
      string to_string () const;
      ulong to_ulong() const;
      int to_int() const;
      float to_float() const;
      bitstring substring(size_t len);
      bitstring substring(size_t start, size_t len);
      void resize(const size_t& n);
      void set(const size_t& i, bool v);



     private:
     bs data;
    };

    struct bitstring_hasher {
        size_t operator() (const bitstring &k) const
        {
          return hash<string> () (k.to_string());
        }
    };
}
#endif //_BITSTRING_H_
