#ifndef _POSTINGLIST_H_
#define _POSTINGLIST_H_

#include <iostream>
#include <set>
#include <unordered_map>

using namespace std;

class DocumentItem
{
 public:
  DocumentItem();

  unsigned long getFid() const;
  void setFid(const unsigned long &value);

  float getScore() const;
  void setScore (float value);

  int getOccurrences() const;
  void setOccurrences(int value);

  string getW() const;
  void setW(const string &value);

  float calculateScore(float norm_factor, int dataset_size, int docs_w);

  string getY() const;
  void setY(const string &value);

 private:
  unsigned long fid;
  float score;
  string w;
  string y;
  int occurrences;
};

struct DocumentItemComparator
{
    bool operator() (const DocumentItem& lhs, const DocumentItem& rhs) const
    {
      return lhs.getScore() < rhs.getScore();
    }
};

using DocumentSet = set<DocumentItem,DocumentItemComparator>;
using PostingList = unordered_map<string,DocumentSet>;
using DatasetInfo = unordered_map<string,unordered_map<unsigned long,DocumentItem>>;
void printPostingList (const PostingList & pl);

#endif //_POSTINGLIST_H_
