#ifndef _TRAPDOOR_H_
#define _TRAPDOOR_H_

#include <iostream>
#include <vector>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/vector.hpp>

using namespace std;

struct hash_pair {
    template <class T1, class T2>
    size_t operator()(const pair<T1, T2>& p) const
    {
      auto hash1 = std::hash<T1>{}(p.first);
      auto hash2 = std::hash<T2>{}(p.second);
      return hash1 ^ hash2;
    }
};

class Trapdoor {
 public:
  string first_w, f_k2_w1, fi_k3_w1;
  size_t k;
  unordered_map<size_t, string> other_w;
  unordered_map<pair<size_t, size_t> , string, hash_pair> tokens;
 private:
  friend class boost::serialization::access;

  template <typename Archive>
  void serialize(Archive &ar, const unsigned int version) { ar & first_w & f_k2_w1 & fi_k3_w1 & k & other_w & tokens; }
};

#endif //_TRAPDOOR_H_
