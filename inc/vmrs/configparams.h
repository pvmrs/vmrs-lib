#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>
#include "libconfig.h++"

using namespace libconfig;

class ConfigParams
{
public:
    static ConfigParams* getInstance();
    static ConfigParams* getInstance(const std::string& cfgpath);
  bool getParam(const std::string &path, std::string &value) const;
  bool getParam(const std::string &path, int &value) const;

private:
    ConfigParams();
    explicit ConfigParams(const std::string& cfgpath);
  static ConfigParams* instance;
  Config lcfg;
};

#endif // CONFIG_H
