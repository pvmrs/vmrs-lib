#ifndef _VMRSSIZES_H_
#define _VMRSSIZES_H_

#include <cstdint>
#include <iostream>
#include <fstream>

using namespace std;

class VMRSSizes {
 public:
  VMRSSizes();

  void exportSizes(const string& file_path);
  void importSizes(const string& file_path);
  void printSizes() const;

  uint32_t dic_size{};
  uint32_t dataset_size{}; //n
  uint32_t score_size{}; //TFxIDF size (b)
  uint32_t array_size{}; // 2^d, d > log m
  uint32_t h1_size{}; //log2(n) + b + log2(pairing-1) + 1
  uint32_t f_size{}; //log2(u)
  uint32_t param_size; //lambda
  uint32_t l_size{}; //lambda
  uint32_t pi_size{}; //d,
  uint32_t zp_size{}; //size of G1
  uint32_t fid_size{}; //log2(dataset_size*8)
  uint32_t zr_size{}; //pairing->getElementSize(Type_Zr,false)*8;
  uint32_t max{}; //larger number of files that have the same word
};

#endif //_VMRSSIZES_H_
