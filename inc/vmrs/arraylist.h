#ifndef _ARRAYLIST_H_
#define _ARRAYLIST_H_

#include <iostream>
#include <vector>
#include <fstream>
#include <vmrs/vmrs.h>

using namespace std;
using namespace vmrs;

using Array = vector<pair<string,string>>;
using ArrayList = vector<Array>;

ArrayList importArrayList(const string& arr_path, const uint32_t& max, const uint32_t& bs_size, const uint32_t& rij_size, const size_t& array_size);
bool exportArrayList (const ArrayList & arr, string &out_file);
void printArrayList (const ArrayList & arr, const uint32_t& h1_size, const uint32_t& param_size);

#endif //_ARRAYLIST_H_
