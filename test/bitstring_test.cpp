#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch.hpp>
#include "../inc/vmrs/bitstring.h"
#include "../inc/vmrs/vmrs.h"

TEST_CASE( "bitstring") {

    using namespace std;
    using namespace vmrs;

    SECTION("creating a bitstring") {
        string data = "asd";
        bitstring bs(data, 17);

        REQUIRE( bs.get_bits() == "00111001101100001");
        REQUIRE( bs.size() == 17 );
        REQUIRE( bs.blocks() == 3);

        bitstring bs2(data);
        REQUIRE (bs2.size() == 24);
        REQUIRE(bs2.to_string() == "asd" );
        REQUIRE( bs2.get_bits() == "011001000111001101100001");
        REQUIRE( bs2.blocks() == 3);

        //ulong
        ulong udata = 97; //a in ASCII
        bitstring bs3(udata);

        REQUIRE( bs3.size() == sizeof(ulong)*CHAR_BIT);
        REQUIRE( bs3.get_bits() == "0000000000000000000000000000000000000000000000000000000001100001");
        REQUIRE( bs3.blocks() == sizeof(ulong));
        REQUIRE( bs3.to_ulong() == 97);

        bitstring bs4(udata, 10);
        REQUIRE( bs4.get_bits() == "0001100001");
        REQUIRE( bs4.size() == 10);
        REQUIRE( bs4.blocks() == 2);

        //int
        int idata = 97; //a in ASCII
        bitstring bs5(idata);

        REQUIRE( bs5.size() == sizeof(int)*CHAR_BIT);
        REQUIRE( bs5.get_bits() == "00000000000000000000000001100001");
        REQUIRE( bs5.blocks() == sizeof(int));
        REQUIRE( bs5.to_int() == 97);

        bitstring bs6(idata, 10);
        REQUIRE( bs6.get_bits() == "0001100001");
        REQUIRE( bs6.size() == 10);
        REQUIRE( bs6.blocks() == 2);

        //float
        float fdata = 97.5; //a in ASCII
        bitstring bs7(fdata);

        REQUIRE( bs7.size() == sizeof(float)*CHAR_BIT);
        REQUIRE( bs7.get_bits() == "01000010110000110000000000000000");
        REQUIRE( bs7.blocks() == sizeof(float));
        REQUIRE( bs7.to_float() == 97.5);

        bitstring bs8(fdata, 10);
        REQUIRE( bs8.get_bits() == "0000000000");
        REQUIRE( bs8.size() == 10);
        REQUIRE( bs8.blocks() == 2);

        //char*
        const char* cdata = "asdf";
        bitstring bs9(cdata, 4*CHAR_BIT);

        REQUIRE( bs9.size() == 4*CHAR_BIT);
        REQUIRE( bs9.get_bits() == "01100110011001000111001101100001");
        REQUIRE( bs9.blocks() == 4);
        REQUIRE( bs9.to_string() == "asdf");

        //bool
        bool b = true;
        bitstring b10(b);
        REQUIRE( b10.size() == 1);
        REQUIRE( b10.get_bits() == "1");
        REQUIRE( b10.blocks() == 1);

        bitstring bs11("ab");
        REQUIRE(bs11[0] == true);
        REQUIRE(bs11[1] == false);
        REQUIRE(bs11[2] == false);
        REQUIRE(bs11[3] == false);
        REQUIRE(bs11[4] == false);
        REQUIRE(bs11[5] == true);
        REQUIRE(bs11[6] == true);
        REQUIRE(bs11[7] == false);
        REQUIRE(bs11[8] == false);
        REQUIRE(bs11[9] == true);

        bitstring bs12(true, 5);
        REQUIRE(bs12.size() == 5);
        REQUIRE(bs12.get_bits() == "11111");

    }

    SECTION ("operations on bitstrings") {
        bitstring bs1("a", 6);
        bitstring bs2("a", 6);
        bitstring bs3("a", 5);

        REQUIRE(bs1 == bs2);
        REQUIRE_FALSE(bs1 != bs2);
        REQUIRE_FALSE(bs1 == bs3);
        REQUIRE(bs1 != bs3);

        bitstring bs44("a");
        bitstring bs4("b");
        bs4+=bs44;
        REQUIRE(bs4.to_string() == "ba");
        REQUIRE(bs4.size() == 2*CHAR_BIT);

        bitstring bs45("g",6);
        REQUIRE(bs45.get_bits () == "100111");


        bs44+=bs45;
        REQUIRE(bs44.size() == CHAR_BIT + 6);
        REQUIRE(bs44.get_bits() == "10011101100001");

        bitstring bs5("a");
        bitstring bs6("b");
        bs5^=bs6;
        REQUIRE(bs5.get_bits() ==  "00000011");

        bitstring bs7("a",6);
        bitstring bs8("b",6);
        bs7^=bs8;
        REQUIRE(bs7.get_bits() ==  "000011");

        bitstring bs9("ABCD");
        stringstream ss;
        ss << bs9;
        REQUIRE(ss.str() == bs9.get_bits());

        bitstring bs10("ABCD");
        bitstring bs11 = bs10.substring(16);
        REQUIRE(bs11.to_string() == "AB");
        REQUIRE(bs11.blocks() == 2);
        REQUIRE(bs11.size() == 16);

        bitstring bs12 = bs10.substring(16, CHAR_BIT);
        REQUIRE(bs12.to_string() == "C");
        REQUIRE(bs12.blocks() == 1);
        REQUIRE(bs12.size() == CHAR_BIT);

        bitstring bs13 = bs10.substring(0, 2*CHAR_BIT);
        REQUIRE(bs13.to_string() == "AB");
        REQUIRE(bs13.blocks() == 2);
        REQUIRE(bs13.size() == 2*CHAR_BIT);

        bitstring bs14 = bs10.substring(24, CHAR_BIT);
        REQUIRE(bs14.to_string() == "D");
        REQUIRE(bs14.blocks() == 1);
        REQUIRE(bs14.size() == CHAR_BIT);

        bitstring bs15 = bs10.substring(9, 17);
        REQUIRE(bs15.get_bits() == "00010000110100001");
        REQUIRE(bs15.blocks() == 3);
        REQUIRE(bs15.size() == 17);

        bitstring bs16 = bs10.substring(9, 23);
        REQUIRE(bs16.get_bits() == "01000100010000110100001");

        REQUIRE(bs16.blocks() == 3);
        REQUIRE(bs16.size() == 23);

        /*

        bitstring bs17("abc", 17);
        REQUIRE(bs17.get_bits () == "10000110010001101");

        bitstring bs18(1,3);
        bs17+=bs18;



        //bs17.resize(20);
        REQUIRE(bs17.size() == 20);
        REQUIRE(bs17.get_bits () == "10000110010001101000");
         */

    }



}