#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch.hpp>
#include "../inc/vmrs/lookuptable.h"

TEST_CASE( "lookuptable") {

  using namespace std;
  using namespace vmrs;

  SECTION("creating a p lookup table") {

    pLookupTable lt(32, 32, 32, 1);
    for (size_t i = 0; i < 10; i ++) {
      lt.insert(to_string(i*10), {to_string(i*100), i, "asd"});
    }

    REQUIRE(lt.size() == 10);

    for (size_t i = 0; i < 10; i ++) {
      REQUIRE(lt.find(to_string(i*10)) != lt.end());
    }

    REQUIRE(lt.find(to_string(9)) == lt.end());
  }



}